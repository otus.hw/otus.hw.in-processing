﻿using BenchmarkDotNet.Running;
using System;

namespace SummBenchmarks
{
    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<SummBenchmark>();
        }
    }
}
