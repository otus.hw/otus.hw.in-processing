﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Jobs;
using Otus.HW.In_Processing;
using Perfolizer.Horology;
using System;
using System.Collections.Generic;
using System.Text;

namespace SummBenchmarks
{
    //[Config(typeof(Config))]
    public class SummBenchmark
    {

        //private class Config : ManualConfig
        //{
        //    public Config()
        //    {
        //        // The same, using the .With() factory methods:
        //        AddJob(
        //            Job.Dry
        //            .WithPlatform(Platform.X64)
        //            .WithLaunchCount(5)
        //            .WithId("MySuperJob"));
        //    }
        //}
        int[] arr;

        [Params(100_000, 1_000_000, 10_000_000)]
        public int K;

        [GlobalSetup]
        public void Setup()
        {
            arr = Generator.GenerateInt(K);
        }

        [Benchmark]
        public void OneThreadSumm()
        {
            var summ = new Combiner().Sum(arr);
        }


        [Benchmark]
        public void MultiThreadSumm()
        {
            var summ = new Combiner().SumThread(arr, 5);
        }

        [Benchmark]
        public void LinqSumm()
        {
            var summ = new Combiner().SumLinq(arr);
        }

    }
}
