﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.HW.In_Processing
{
    public class Combiner
    {

        public Int64 Sum(int[] source)
        {
            Int64 summ = 0;
            foreach (var item in source)
            {
                summ += item;
            }

            return summ;
        }

        public Int64 SumLinq(int[] source)
            => source.AsParallel().Sum();


        public int SumThread(int[] source, int threadpool)
        {
            var summ = 0;
            // Размер chunk'ов
            var size = Math.Ceiling((double)source.Length / threadpool);
            //Делим на chunk'и
            var chunks = ToChunk(source, (int)size);

            var threads = new List<Thread>();
            // Обрабатываем порцию в отдельном потоке
            foreach (var chunk in chunks)
            {

                Thread thread = new Thread(() =>
                {
                    var s = 0;
                    foreach (var item in chunk)
                    {
                        s += item;
                    }
                    Interlocked.Add(ref summ, s);
                });
                threads.Add(thread);
                thread.Start();
            }

            foreach (var item in threads)
            {
                item.Join();
            }
            return summ;
        }

        private IEnumerable<int[]> ToChunk(IEnumerable<int> source,
                int size)
        {
            using (var iter = source.GetEnumerator())
            {
                while (iter.MoveNext())
                {
                    var chunk = new int[size];
                    int count = 1;
                    chunk[0] = iter.Current;
                    for (int i = 1; i < size && iter.MoveNext(); i++)
                    {
                        chunk[i] = iter.Current;
                        count++;
                    }
                    if (count < size)
                    {
                        Array.Resize(ref chunk, count);
                    }
                    yield return chunk;
                }
            }
        }

    }
}
