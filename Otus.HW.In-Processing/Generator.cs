﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.HW.In_Processing
{
    public static class Generator
    {
        public static int[] GenerateInt(int size)
        {
            var arr = new int[size];
            for (int i = 0; i < size; i++)
            {
                arr[i] = 2;
            }
            return arr;
        }
    }
}
