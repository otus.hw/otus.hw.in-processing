﻿using System;
using System.Diagnostics;

namespace Otus.HW.In_Processing
{
    class Program
    {
        static void Main(string[] args)
        {
            var param = new int[3] { 100_000, 1_000_000, 10_000_000};
            foreach (var K in param)
            {


                var arr = Generator.GenerateInt(K);

                Stopwatch timer = new Stopwatch();
                timer.Start();
                Combiner combiner = new Combiner();
                var summ = combiner.Sum(arr);
                timer.Stop();

                Console.WriteLine($"Summ: {summ}, Time summ 1 thread: {timer.Elapsed}");

                timer.Restart();
                summ = combiner.SumThread(arr,5);
                timer.Stop();

                Console.WriteLine($"Summ: {summ}, Time summ MultiThread: {timer.Elapsed}");

                timer.Restart();
                summ = combiner.SumLinq(arr);
                timer.Stop();

                Console.WriteLine($"Summ: {summ}, Time summ LINQ: {timer.Elapsed}");
            }
        }
    }
}
